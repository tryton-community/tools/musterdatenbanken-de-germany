Diese Tryton-Musterdatenbanken sind für den Gebrauch mit dem Installationsskript [verfügbar hier](https://foss.heptapod.net/tryton-community/tools/tryton-installation-and-maintainance-scripts-for-pip-venv) gedacht und können aus diesem heraus abgerufen werden. Eine manuelle Installation ist ebenfalls möglich.

Sie enthalten wesentliche Vorkonfigurationen wie Geschäftsjahr, Nummernkreis, Musterfirma, Musterartikel und Musterkunden, Buchungssatzvorlagen, vorinstallierte Standard-Kontenrahmen u.v.m. So kann Tryton innerhalb weniger Minuten betriebsbereit gemacht werden.


These preconfigured databases for Tryton do contain specific German settings. They are intended to be called from the install script; however they can be used within a manual install process as well.
